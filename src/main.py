from bs4 import BeautifulSoup
from urllib.request import urlopen
from pystray import Icon, MenuItem
from PIL import Image
import io

COVID_URL = "https://www.appstate.edu/go/coronavirus/"
APP_NAME = "App State Coronavirus"


def getNumbers(url):
    # get HTML
    with urlopen(url) as fp:
        bs = BeautifulSoup(fp, features="lxml")

    # get the tables
    tables = bs.find_all(class_="report-table")
    activeTable = tables[0]
    cumulativeTable = tables[1]

    # parse active cases
    activeRows = activeTable.find_all("tr")
    activeNumbers = activeRows[2].find_all("td")
    activeStudents = activeNumbers[0].get_text()
    activeEmployees = activeNumbers[1].get_text()
    activeSubcontractors = activeNumbers[2].get_text()

    # parse cumulative cases
    cumulativeRows = cumulativeTable.find_all("tr")
    cumulativeNumbers = cumulativeRows[2].find_all("td")
    cumulativeStudents = cumulativeNumbers[0].get_text()
    cumulativeEmployees = cumulativeNumbers[1].get_text()
    cumulativeSubcontractors = cumulativeNumbers[2].get_text()

    # return case numbers
    return activeEmployees, activeStudents, activeSubcontractors, cumulativeEmployees, cumulativeStudents, cumulativeSubcontractors


def getImage():
    # get HTML
    with urlopen(COVID_URL) as fp:
        bs = BeautifulSoup(fp, features="lxml")
    # get image
    imageClass = bs.find(class_="header-icon")
    imageTag = imageClass.find("img")
    imageSRC = imageTag.get("src")
    imageURL = "https://www.appstate.edu/go/coronavirus/" + imageSRC
    # download image
    imageRaw = None
    with urlopen(imageURL) as x:
        imageRaw = bytearray(x.read())
    # decode image
    imgPillow = Image.open(io.BytesIO(imageRaw))
    # return image
    return imgPillow


def onClick():
    print("PING")


def stop():
    exit()


aEm, aSt, aSu, cEm, cSt, cSu = getNumbers(COVID_URL)
menu = [
    MenuItem('Active Students: %s' % (aSt), onClick),
    MenuItem('Active Employees: %s' % (aEm), onClick),
    MenuItem('Active Subcontractors: %s' % (aSu), onClick),
    MenuItem('Cumulative Students: %s' % (cSt), onClick),
    MenuItem('Cumulative Employees: %s' % (cEm), onClick),
    MenuItem('Cumulative Subcontractors: %s' % (cSu), onClick),
    MenuItem('Exit', stop)
]
app = Icon(APP_NAME, getImage(), menu=menu)

try:
    app.run()
except KeyboardInterrupt:
    stop()
